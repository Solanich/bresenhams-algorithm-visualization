# Tarea 2b

Implementación con PyOpenGL del Algorítmo de Bresenham para el dibujo de líneas en raster displays

![Screenshot_1.png](https://bitbucket.org/repo/BgqdnA7/images/3386431714-Screenshot_1.png)

## Requisitos

Se requieren las siguientes dependencias para ejecutar el programa:

1. [Python 3](https://www.anaconda.com/download/)
2. [Pygame](https://www.pygame.org/)
3. [PyOpenGL](http://pyopengl.sourceforge.net/)

## Instalación

Solo basta tener una copia del repositorio. No hay pasos extra para instalar este programa.

## Ejecución

Mediante consola ejecutar `python main.py axb x0,y0 x1,y1 (r0,g0,b0) (r1,g1,b1)`

* `a` - Dimensión horizontal de la grilla
* `b` - Dimensión vertical de la grilla
* `x0,y0` - Coordenadas del punto incial, entero
* `x1,y1` - Coordenadas del punto final, entero
* `(r0,g0,b0)` - Color inicial en rgb, rango desde 0 a 1
* `(r1,g1,b1)` - Color final en rgb, rango desde 0 a 1

Considerar el origen del sistema en la esquina inferior izquierda.

## Autor

Sebastián Solanich
# coding=utf-8
import time
import sys
import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from bresenham import bresenham, colorPixels

def draw2Vertex3fv(a, b): #lista linea
    glVertex3fv(a)
    glVertex3fv(b)

def draw4Vertex3fv(a, b, c, d): #lista cuadrado
    glVertex3fv(a)
    glVertex3fv(b)
    glVertex3fv(c)
    glVertex3fv(d)

def axesList(h): #lista que genera ejes
    x = [h, 0, 0]
    y = [0, h, 0]
    z = [0, 0, h]
    o = [0, 0, 0]
    
    axes = glGenLists(1)
    glNewList(axes, GL_COMPILE)
    
    glBegin(GL_LINES)
    
    glColor3fv([1, 0, 0])
    draw2Vertex3fv(o, x)
    
    glColor3fv([0, 1, 0])
    draw2Vertex3fv(o, y)
    
    glColor3fv([0, 0, 1])
    draw2Vertex3fv(o, z)
    
    glEnd()
    glEndList()
    
    return axes

def coloredCubeList(color=[0.2,0.2,0.2]): #lista que genera cubo
    offset = 0.1
    a = [-0.5,-0.5,-0.5]
    b = [ 0.5,-0.5,-0.5]
    c = [ 0.5, 0.5,-0.5]
    d = [-0.5, 0.5,-0.5]
    e = [-0.5,-0.5, 0.5]
    f = [ 0.5,-0.5, 0.5]
    g = [ 0.5, 0.5, 0.5]
    h = [-0.5, 0.5, 0.5]  
    cube = glGenLists(1)
    glNewList(cube, GL_COMPILE)
    glBegin(GL_QUADS)
    glColor3fv(color)
    draw4Vertex3fv(a,b,c,d)
    color[0] += offset
    color[1] += offset
    color[2] += offset
    glColor3fv(color)
    draw4Vertex3fv(b,f,g,c)
    color[0] += offset
    color[1] += offset
    color[2] += offset
    glColor3fv(color)
    draw4Vertex3fv(f,e,h,g)
    color[0] += offset
    color[1] += offset
    color[2] += offset
    glColor3fv(color)
    draw4Vertex3fv(e,a,d,h)
    color[0] += offset
    color[1] += offset
    color[2] += offset
    glColor3fv(color)
    draw4Vertex3fv(d,c,g,h)
    color[0] += offset
    color[1] += offset
    color[2] += offset
    glColor3fv(color)
    draw4Vertex3fv(a,e,f,b)
    color[0] += offset
    color[1] += offset
    color[2] += offset
    glEnd()
    glEndList()
    return cube

# dibuja una linea entre los pixeles (x0, y0) y (x1, y1)
def draw_line(x0, y0, x1, y1, color): #(int, int, int, int, array) en pixeles
    glLineWidth(2)
    line = glGenLists(1)
    glNewList(line, GL_COMPILE)

    glColor3fv(color)
    glBegin(GL_LINES)
    draw2Vertex3fv([2*x0/WIDTH, 2*y0/HEIGHT, 0], [2*x1/WIDTH, 2*y1/HEIGHT, 0])
    glEnd()
    glEndList()
    return line

# genera lista de un cuadrado en (x0, y0) de largo l
def rectList(x0, y0, l=8, color=[1,0,0]):
    a = [2*x0/WIDTH, 2*y0/HEIGHT, 0]
    b = [2*(x0 + l)/WIDTH, 2*y0/HEIGHT, 0]
    c = [2*(x0 + l)/WIDTH, 2*(y0 + l)/HEIGHT, 0]
    d = [2*x0/WIDTH, 2*(y0 + l)/HEIGHT, 0]
    
    square = glGenLists(1)
    glNewList(square, GL_COMPILE)
    glBegin(GL_QUADS)
    glColor3fv(color)
    draw4Vertex3fv(a,b,c,d) ## hace superficie cuadrada
    glEnd()
    glEndList()
    return square

def cubeList(x0, y0, l=8, color=[1,0,0]):
    a = [8*x0/WIDTH, 8*y0/HEIGHT, 7]
    b = [8*x0 + l/WIDTH, 8*y0/HEIGHT, 7]
    c = [8*x0 + l/WIDTH, 8*(y0 + l)/HEIGHT, 7]
    d = [8*x0/WIDTH, 8*(y0 + l)/HEIGHT, 7]
    e = [8*x0/WIDTH, 8*y0/HEIGHT, (8*l)+7]
    f = [8*x0 + l/WIDTH, 8*y0/HEIGHT, (8*l)+7]
    g = [8*x0 + l/WIDTH, 8*(y0 + l)/HEIGHT, (8*l)+7]
    h = [8*x0/WIDTH, 8*(y0 + l)/HEIGHT, (8*l)+7]
    
    cube = glGenLists(1)
    glNewList(cube, GL_COMPILE)
    glBegin(GL_QUADS)
    glColor3fv(color)
    draw4Vertex3fv(d,c,g,h)
    draw4Vertex3fv(b,f,g,c)
    draw4Vertex3fv(a,b,c,d)
    draw4Vertex3fv(a,e,f,b)
    draw4Vertex3fv(f,e,h,g)
    draw4Vertex3fv(e,a,d,h)
    glEnd()
    glEndList()
    return cube

def grid(x, y): # dibuja grilla de dimension x,y
    gridLists = []
    for i in range(x+1): #verticales
        gridLists.append(draw_line(i*SPACE, 0, i*SPACE, SPACE*y, [0.274,0.274,0.274]))
    for j in range(y+1): #horizontales
        gridLists.append(draw_line(0, j*SPACE, SPACE*x, j*SPACE, [0.274,0.274,0.274]))
    return gridLists

# pinta celda en coordenada i,j
def plot(i, j, color): #[60,179,113]
    xcoord = i * SPACE
    ycoord = j * SPACE
    return rectList(xcoord, ycoord, SPACE, color)
def plot2(point_list, color_list):
    dotlist = []
    cublist = []
    for i in range(len(point_list)):
        dotlist.append(plot(point_list[i][0], point_list[i][1], color_list[i]))
        cublist.append(cubeList(point_list[i][0], point_list[i][1], color=color_list[i]))
    return dotlist, cublist

def cam1(): # top view 2d
    gluLookAt( 400, 400, 1000.0, \
                   400, 400, 0.0, \
                   0.0, 1.0, 0.0)
def cam2(): #angle view
    gluLookAt( 1300.0, 1300.0, 1300.0, \
                   0.0, 0.0, 0.0, \
                   0.0, 0.0, 1.0)

CAM1 = 0
CAM2 = 1

def setupProjection(WIDTH, HEIGHT): # CAMARA Y ANGULO
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    d = 0.5#0.5
    glFrustum(-d, d, -d, d, 1, 5000)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

# Imprime el paso actual, el punto a pintar y la variable de decision
def printStatus(step, points, pList):
    if step == len(points):
        nextPoint = "| [Punto final]"
    else:
        nextPoint = points[step]
    if nextPoint == "| [Punto final]":
        print("Paso:", step, nextPoint, "| Variable de Decision:", pList[step-1])
    else:
        print("Paso:", step, "| Proximo pixel:", nextPoint, "| Variable de Decision:", pList[step-1])

# convierte los argumentos pasados por consola a variables
def parseArgs(args):
    dimensiones = args[1].split('x')
    dimx = int(dimensiones[0])
    dimy = int(dimensiones[1])

    punto0 = args[2].split(',')
    p0_x = int(punto0[0])
    p0_y = int(punto0[1])

    punto1 = args[3].split(',')
    p1_x = int(punto1[0])
    p1_y = int(punto1[1])

    color0 = args[4][1:len(args[4])-1].split(',')
    c0_r = float(color0[0])
    c0_g = float(color0[1])
    c0_b = float(color0[2])

    color1 = args[5][1:len(args[5])-1].split(',')
    c1_r = float(color1[0])
    c1_g = float(color1[1])
    c1_b = float(color1[2])

    return [dimx, dimy], [p0_x, p0_y], [p1_x, p1_y], [c0_r, c0_g, c0_b], [c1_r, c1_g, c1_b]

if __name__ == '__main__':
    dim, p0, p1, c0, c1 = parseArgs(sys.argv)
    WIDTH = 800
    HEIGHT = 600

    #lista de puntos bresenham
    point_list, pList = bresenham(p0[0], p0[1], p1[0], p1[1])
    color_list = colorPixels(point_list, c0, c1)
    
    SPACE = 8 #espacio entre grillas
    cam = CAM1
    run = True
    t0 = time.time()
    pygame.init()
    pygame.display.set_mode((WIDTH, HEIGHT), OPENGL | DOUBLEBUF)
    pygame.display.set_caption("Main")

    # listas
    axes = axesList(100000)
    gridlist = grid(dim[0],dim[1])
    squarelist, cubelist = plot2(point_list, color_list)
    
    # background
    glClearColor(0.941, 0.941, 0.941, 0.941)

    # depth
    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LEQUAL)

    # ventana
    glViewport(0, 0, WIDTH, HEIGHT)
    step = 1
    while run:
        t1 = time.time()
        dt = t1 - t0
        t0 = t1
        for event in pygame.event.get():
            if event.type == QUIT:
                run = False
            elif event.type == KEYDOWN:
                if event.key == K_1:
                    cam = CAM1
                elif event.key == K_2:
                    cam = CAM2
                if event.key == K_RIGHT:
                    step += 1
                    if step > len(point_list):
                        step = len(point_list)
                    else:
                        printStatus(step, point_list, pList)
                if event.key == K_LEFT:
                    step -= 1
                    if step < 1:
                        step = 1
                    else:
                        printStatus(step, point_list, pList)
                else:
                    continue

        # clear buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # projection
        setupProjection(WIDTH, HEIGHT)

        # Setting up the camera
        if cam == CAM1:
            cam1()
        elif cam == CAM2:
            cam2()

        glCallList(axes)

        #pintar celdas
        for square in squarelist[0:step]:
            glPushMatrix()
            glScale(1600, 1600, 1600)
            glCallList(square)
            glPopMatrix()
        #dibujar grilla
        for line in gridlist:
            glPushMatrix()
            glScale(1600, 1600, 1600)
            glCallList(line)
            glPopMatrix()

        pygame.display.flip()
        pygame.time.wait(int(1000 / 30))

    pygame.quit()